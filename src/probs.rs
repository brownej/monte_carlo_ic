use crate::{
    data::{
        kinematics::{self as kin_data, kin_line},
        nuc::{Reaction, ReactionLevel, LEVELS},
        runs::{Energy, Event, RUN_INFO},
    },
    e_loss::{self, all_e_losses},
    output::{McOutput, McSectionOutput, McSpeciesOutput, INCLUDE_DATA},
    statistics::{randomize, stats},
};
use kincal::{Outgoing, Value};
use rand::thread_rng;
use statrs::function::erf::erf;
use std::{cmp::Ordering, collections::HashMap};

const IC_RES: [f64; 4] = [0.093, 0.112, 0.110, 0.110];

pub fn event_probs(event: &Event, energy: Energy) -> (HashMap<Reaction, f64>, McOutput) {
    use kin_data::{AR34AP, CL34AP, S34AP};

    let mut probs = HashMap::new();
    let mut outputs = HashMap::new();

    for r in [S34AP, CL34AP, AR34AP] {
        let (raw_prob, output) = raw_prob(event, energy, r);
        probs.insert(r, raw_prob);
        outputs.insert(r.beam, output);
    }

    let norm = probs.iter().map(|(_, prob)| prob).sum::<f64>();
    probs.iter_mut().for_each(|(_, x)| {
        *x /= norm;
    });

    (probs, McOutput(outputs))
}

pub fn closest_level_thcm(event: &Event, energy: f64, reac: Reaction) -> (u32, f64) {
    let energy_e = event.si_tot_energy.expect("no si_tot_energy") / 1000.0;
    let theta_e = event.si_theta.expect("no si_theta");

    let level_thcm = LEVELS[&reac.recoil]
        .iter()
        .enumerate()
        .map(|(l, _)| {
            let kin = kin_line(ReactionLevel(reac, l as u32), energy);

            match kin.th_to_thcm(theta_e, Outgoing::Ejectile) {
                Value::NoVal => ((l, f64::NAN), f64::NAN),
                Value::OneVal(x) => (
                    (l, x),
                    (energy_e - kin.thcm_to_k(x, Outgoing::Recoil)).abs(),
                ),
                Value::TwoVal(x, y) => {
                    let diff_x = (kin.thcm_to_k(x, Outgoing::Recoil)).abs();
                    let diff_y = (kin.thcm_to_k(y, Outgoing::Recoil)).abs();
                    if diff_x < diff_y {
                        ((l, x), diff_x)
                    } else {
                        ((l, y), diff_y)
                    }
                }
            }
        })
        .min_by(|(_, a), (_, b)| match (a.is_nan(), b.is_nan()) {
            (true, true) => Ordering::Equal,
            (true, false) => Ordering::Greater,
            (false, true) => Ordering::Less,
            (false, false) => a
                .partial_cmp(b)
                .expect("failed partial_cmp, even though NaN should be handled"),
        })
        .expect("failed to find minimum of whole")
        .0;

    (level_thcm.0 as u32, level_thcm.1)
}

// What this does is:
// 1. use the ejectile info to determine which excited level is closest
// 2. get the energy loss of the beam through part of the jet
// 3. determine the recoil info from the kinematics and the ejectile info
// 4. get the energy loss through the rest of the jet and the IC
// 5. compare that to the experimental energy losses
pub fn raw_prob(event: &Event, energy: Energy, r: Reaction) -> (f64, McSpeciesOutput) {
    const N_ITER: usize = 1000;

    let run_info = &RUN_INFO[event.run_name.as_ref().expect("no run_name")];

    let ic_losses_exp = vec![
        event.ic_x_energy.map(|x| x / 1000.0),
        event.ic_y_energy.map(|x| x / 1000.0),
        event.ic_de_energy.map(|x| x / 1000.0),
        event.ic_e_energy.map(|x| x / 1000.0),
    ];

    let mut ic_losses_theor = vec![
        Vec::with_capacity(N_ITER),
        Vec::with_capacity(N_ITER),
        Vec::with_capacity(N_ITER),
        Vec::with_capacity(N_ITER),
    ];

    for _ in 0..N_ITER {
        let mut rng = thread_rng();

        let (jet_before, jet_after) = e_loss::jet(run_info.rhoa);

        let e_init = energy.randomized(r.beam.into());
        let mut e_losses = vec![];
        let mut de = 0.0;

        let mut e_losses_before = all_e_losses(r.beam, e_init - de, &[jet_before]);
        de += e_losses.iter().map(|x: &(_, _, _, f64)| x.3).sum::<f64>();
        e_losses.append(&mut e_losses_before);

        let (closest_level, theta_cm) = closest_level_thcm(event, e_init - de, r);
        let kin = kin_line(ReactionLevel(r, closest_level), e_init - de);
        //let theta_r_pred = kin.thcm_to_th(theta_cm, Outgoing::Recoil);
        let energy_r_pred = kin.thcm_to_k(theta_cm, Outgoing::Recoil);

        let e_init = energy_r_pred;
        let mut de = 0.0;

        let mut e_losses_after = all_e_losses(r.recoil, e_init - de, &[jet_after]);
        de += e_losses_after
            .iter()
            .map(|x: &(_, _, _, f64)| x.3)
            .sum::<f64>();
        e_losses.append(&mut e_losses_after);

        // FIXME: Don't use a default
        let ic = e_loss::ic(run_info.cap_ic.expect("no cap_ic data for run"));
        //let ic = e_loss::ic(run_info.cap_ic.unwrap_or((15.0, 0.0, 1.0)));
        let mut e_losses_ic = all_e_losses(r.recoil, e_init - de, &ic);
        e_losses.append(&mut e_losses_ic);

        ic_losses_theor[0].push(randomize(
            (e_losses[e_losses.len() - 4].3, 0.0, IC_RES[0]),
            &mut rng,
        ));
        ic_losses_theor[1].push(randomize(
            (e_losses[e_losses.len() - 3].3, 0.0, IC_RES[1]),
            &mut rng,
        ));
        ic_losses_theor[2].push(randomize(
            (e_losses[e_losses.len() - 2].3, 0.0, IC_RES[2]),
            &mut rng,
        ));
        ic_losses_theor[3].push(randomize(
            (e_losses[e_losses.len() - 1].3, 0.0, IC_RES[3]),
            &mut rng,
        ));
    }

    let mut section_outputs = HashMap::new();

    // for each ic section, there is an optional experimental energy loss (not all sections have
    // data), and a vec of N_ITER thoretical energy losses.
    // we iterate through the sections, and compare the experimental value to the theoretical
    // distribution to get a raw probability for that section.
    let section_probs = ic_losses_theor
        .iter_mut()
        .zip(ic_losses_exp.iter())
        .enumerate()
        .map(|(section_num, (theor, exp))| {
            exp.map(|exp| {
                // after sorting, theor's values vs their indices (+1) should be the CDF of their
                // distribution.
                // assuming they're normally distributed, calculate the probability using erf.
                theor.sort_by(|a, b| a.partial_cmp(b).expect("unable to sort theor values"));
                let (mean, std_dev) = stats(theor);
                let func = |x| (N_ITER as f64) / 2.0 * (1.0 + erf(x / std_dev / f64::sqrt(2.0)));

                // to get the probability, you want the integral of the gaussian on both sides of
                // the peak
                let raw_prob = 2.0 * func(-(exp - mean).abs());

                let section_name = match section_num {
                    0 => "X",
                    1 => "Y",
                    2 => "dE",
                    3 => "E",
                    _ => unreachable!("there are only 4 sections"),
                }
                .to_string();
                section_outputs.insert(
                    section_name,
                    McSectionOutput {
                        data: None,
                        gaus_params: (mean, std_dev),
                        raw_prob,
                    },
                );

                raw_prob
            })
        })
        .collect::<Vec<_>>();

    if INCLUDE_DATA {
        for (section_num, data) in ic_losses_theor.into_iter().enumerate() {
            let section_name = match section_num {
                0 => "X",
                1 => "Y",
                2 => "dE",
                3 => "E",
                _ => unreachable!("there are only 4 sections"),
            };

            section_outputs
                .get_mut(section_name)
                .expect("section doesn't exist")
                .data = Some(data);
        }
    }

    // take the average probability of the available sections (not all sections have an
    // experimental value)
    let prob = (section_probs.iter().filter_map(|x| *x).sum::<f64>())
        / (section_probs.iter().filter_map(|x| *x).count() as f64);

    (
        prob,
        McSpeciesOutput {
            prob,
            sections: section_outputs,
        },
    )
}
