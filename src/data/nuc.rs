use crate::data::constants::ATOMIC_MASS_UNIT as u;
use e15232_srim::Projectile;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Beam {
    S34,
    Cl34,
    Ar34,
}

impl From<Nuclide> for Beam {
    fn from(nuc: Nuclide) -> Self {
        match nuc {
            Nuclide::S34 => Self::S34,
            Nuclide::Cl34 => Self::Cl34,
            Nuclide::Ar34 => Self::Ar34,
            _ => panic!("unknown beam"),
        }
    }
}

impl From<Nuclide> for Projectile {
    fn from(nuc: Nuclide) -> Self {
        match nuc {
            Nuclide::S34 => Self::S34,
            Nuclide::Cl34 => Self::Cl34,
            Nuclide::Ar34 => Self::Ar34,
            Nuclide::Cl37 => Self::Cl37,
            Nuclide::Ar37 => Self::Ar37,
            Nuclide::K37 => Self::K37,
            _ => panic!("unknown projectile"),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[non_exhaustive]
pub enum Nuclide {
    #[serde(rename = "p")]
    H1,
    #[serde(rename = "a")]
    He4,
    #[serde(rename = "34S")]
    S34,
    #[serde(rename = "34Cl")]
    Cl34,
    #[serde(rename = "34Ar")]
    Ar34,
    #[serde(rename = "36Ar")]
    Ar36,
    #[serde(rename = "37Cl")]
    Cl37,
    #[serde(rename = "37Ar")]
    Ar37,
    #[serde(rename = "37K")]
    K37,
}

impl From<Beam> for Nuclide {
    fn from(beam: Beam) -> Self {
        match beam {
            Beam::S34 => Self::S34,
            Beam::Cl34 => Self::Cl34,
            Beam::Ar34 => Self::Ar34,
        }
    }
}

impl Nuclide {
    pub fn mass(&self) -> f64 {
        match self {
            Self::H1 => 1.00782503224 * u,
            Self::He4 => 4.00260325413 * u,
            Self::S34 => 33.967867012 * u,
            Self::Cl34 => 33.973762491 * u,
            Self::Ar34 => 33.980270093 * u,
            Self::Ar36 => 35.967545105 * u,
            Self::Cl37 => 36.965902584 * u,
            Self::Ar37 => 36.966776314 * u,
            Self::K37 => 36.973375889 * u,
        }
    }
}

lazy_static! {
    pub static ref LEVELS: HashMap<Nuclide, Vec<f64>> = {
        use crate::data::nuc::Nuclide::*;

        let mut levels = HashMap::new();
        levels.insert(Ar36, get_levels(include_str!("nuc/levels_36Ar.dat"), 10));
        levels.insert(Cl37, get_levels(include_str!("nuc/levels_37Cl.dat"), 2));
        levels.insert(Ar37, get_levels(include_str!("nuc/levels_37Ar.dat"), 84));
        levels.insert(K37, get_levels(include_str!("nuc/levels_37K.dat"), 91));
        levels
    };
}

fn get_levels(s: &str, n: usize) -> Vec<f64> {
    s.lines()
        .take(n)
        .map(str::parse::<f64>)
        .collect::<Result<Vec<_>, _>>()
        .unwrap()
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ReactionLevel(pub Reaction, pub u32);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Reaction {
    pub beam: Nuclide,
    pub target: Nuclide,
    pub ejectile: Nuclide,
    pub recoil: Nuclide,
}
