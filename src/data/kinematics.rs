use crate::data::nuc::{self as nuc_data, Nuclide, Reaction, ReactionLevel};
use kincal::ReactionKinematics;

pub const S34AP: Reaction = Reaction {
    beam: Nuclide::S34,
    target: Nuclide::He4,
    ejectile: Nuclide::H1,
    recoil: Nuclide::Cl37,
};
pub const CL34AP: Reaction = Reaction {
    beam: Nuclide::Cl34,
    target: Nuclide::He4,
    ejectile: Nuclide::H1,
    recoil: Nuclide::Ar37,
};
pub const AR34AP: Reaction = Reaction {
    beam: Nuclide::Ar34,
    target: Nuclide::He4,
    ejectile: Nuclide::H1,
    recoil: Nuclide::K37,
};
pub const AR34AP0: ReactionLevel = ReactionLevel(AR34AP, 0);

pub fn kin_line(r: ReactionLevel, beam_energy: f64) -> ReactionKinematics {
    ReactionKinematics::new(
        [
            r.0.beam.mass(),
            r.0.target.mass(),
            r.0.ejectile.mass(),
            r.0.recoil.mass() + nuc_data::LEVELS[&r.0.recoil][r.1 as usize],
        ],
        beam_energy,
    )
}
