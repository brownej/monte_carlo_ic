use crate::{data::nuc::Beam, statistics::randomize};
use lazy_static::lazy_static;
use rand::thread_rng;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs::File, io::BufReader};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Energy {
    E1,
    E2,
}

impl Energy {
    /// Total kinetic energy of the beams before the target \[Mev].
    pub fn energy_actual(&self, beam: Beam) -> f64 {
        match (self, beam) {
            (Self::E1, Beam::S34) => 57.02,  // +- 0.75
            (Self::E1, Beam::Cl34) => 57.03, // +- 0.75
            (Self::E1, Beam::Ar34) => 57.04, // +- 0.75
            (Self::E2, Beam::S34) => 54.17,  // +- 0.73
            (Self::E2, Beam::Cl34) => 54.18, // +- 0.73
            (Self::E2, Beam::Ar34) => 54.19, // +- 0.73
        }
    }

    /// Nominal beam energy per nucleon before the target \[Mev/u].
    pub fn energy_per_nuc_nominal(&self) -> f64 {
        match self {
            Self::E1 => 1.71,
            Self::E2 => 1.625,
        }
    }

    pub fn randomized(&self, beam: Beam) -> f64 {
        let mut rng = thread_rng();

        let val = self.energy_actual(beam);
        let unc = match self {
            Self::E1 => 0.75,
            Self::E2 => 0.73,
        };

        randomize((val, 0.0, unc), &mut rng)
    }
}

lazy_static! {
    /// The event data for each energy.
    pub static ref RECOILS: HashMap<Energy, Recoils> = {
        let mut map = HashMap::new();
        map.insert(
            Energy::E1,
            {
                let file = File::open("recoils_E1.json").expect("failed to read recoils_E1.json");
                let file = BufReader::new(file);
                serde_json::from_reader(file).expect("failed to parse recoils_E1.json")
            }
        );
        map.insert(
            Energy::E2,
            {
                let file = File::open("recoils_E2.json").expect("failed to read recoils_E2.json");
                let file = BufReader::new(file);
                serde_json::from_reader(file).expect("failed to parse recoils_E2.json")
            }
        );

        map
    };

    /// A list of runs for each energy.
    pub static ref RUN_LISTS: HashMap<Energy, Vec<String>> = {
        let mut map = HashMap::new();
        map.insert(
            Energy::E1,
            {
                let file = File::open("runs_E1.json").expect("failed to open runs_E1.json");
                let file = BufReader::new(file);
                serde_json::from_reader(file).expect("failed to parse runs_E1.json")
            }
        );
        map.insert(
            Energy::E2,
            {
                let file = File::open("runs_E2.json").expect("failed to open runs_E2.json");
                let file = BufReader::new(file);
                serde_json::from_reader(file).expect("failed to parse runs_E2.json")
            }
        );

        map
    };

    /// The number of (a, 2p) events for each energy.
    pub static ref NUM_2P: HashMap<Energy, (f64, f64, f64)> = {
        let mut map = HashMap::new();
        map.insert(Energy::E1, 5.0);
        map.insert(Energy::E2, 6.0);

        map.into_iter().map(|(k, v)| (k, (v, f64::sqrt(v), 0.0))).collect()
    };

    /// The 34S(a,p) cross section \[cm^2] at each energy.
    /// From A. F. Scott et al., Nucl. Phys. A 552, 363 (1993)
    pub static ref XSEC_34S: HashMap<Energy, (f64, f64, f64)> = {
        let mut map = HashMap::new();
        #[allow(clippy::excessive_precision)]
        map.insert(Energy::E1, (12.958618667161318e-27, 0.0, 0.8e-27));
        #[allow(clippy::excessive_precision)]
        map.insert(Energy::E2, (11.118579955435905e-27, 0.0, 0.8e-27));

        map
    };

    /// Run info for each run.
    pub static ref RUN_INFO: HashMap<String, RunInfo> = {
        let file = File::open("run_info.json").expect("failed to open run_info.json");
        let file = BufReader::new(file);
        serde_json::from_reader(file).expect("failed to parse run_info.json")
    };

    /// Fractions of each beam type for each run.
    pub static ref BEAM_FRACS: HashMap<String, BeamFraction> = {
        let file = File::open("beam_fractions.json").expect("failed to open beam_fractions.json");
        let file = BufReader::new(file);
        serde_json::from_reader(file).expect("failed to parse beam_fractions.json")
    };

    /// Scaler info for each run.
    pub static ref SCALERS: HashMap<String, (f64, f64)> = {
        let file = File::open("scalers.json").expect("failed to open scalers.json");
        let file = BufReader::new(file);
        serde_json::from_reader(file).expect("failed to parse scalers.json")
    };

    /// The total solid angle coverage of the silicon detectors.
    pub static ref SOLID_ANGLE: (f64, f64) = (3.766697374700109, 0.5310730696186715);

    /// The detection efficiency of 34S(a,p).
    pub static ref EFF_34S: (f64, f64, f64) = {
        let eff = 0.2643;
        (eff, 0.0005, eff * SOLID_ANGLE.1 / SOLID_ANGLE.0)
    };

    /// The detection efficiency of 34Cl(a,p) and 34Ar(a,p).
    pub static ref EFF_34CL_34AR: (f64, f64, f64) = {
        let eff_34cl = 0.272815;
        let eff_34ar = 0.273364;
        let eff_avg = (eff_34cl + eff_34ar) / 2.0;
        let eff_diff = f64::abs(eff_34cl - eff_34ar);
        (eff_avg, 0.0005, eff_diff + (eff_avg * SOLID_ANGLE.1 / SOLID_ANGLE.0))
    };

    /// The detection efficiency of 34Cl(a,p0) and 34Ar(a,p0).
    pub static ref EFF_34CL_34AR_P0: (f64, f64, f64) = {
        let eff = 0.2768;
        (eff, 0.005, 0.0002 + eff * SOLID_ANGLE.1 / SOLID_ANGLE.0)
    };
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RunInfo {
    pub run_energy: String,
    pub start_time: f64,
    pub stop_time: f64,
    pub cap_ic: Option<(f64, f64, f64)>,
    pub cap_in: (f64, f64, f64),
    pub rhoa: (f64, f64, f64),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Ejectile {
    #[serde(rename = "a")]
    Alpha,
    #[serde(rename = "p")]
    Proton,
    #[serde(rename = "?")]
    Unknown,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Recoil {
    #[serde(rename = "34s")]
    S34,
    #[serde(rename = "34cl")]
    Cl34,
    #[serde(rename = "34ar")]
    Ar34,
    #[serde(rename = "37cl")]
    Cl37,
    #[serde(rename = "37ar")]
    Ar37,
    #[serde(rename = "37k")]
    K37,
    #[serde(rename = "?")]
    Unknown,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Event {
    pub run_name: Option<String>,
    pub run_pressure: Option<f64>,
    pub event_id: Option<u64>,
    pub ejectile_type: Option<Ejectile>,
    pub recoil_type: Option<Recoil>,
    pub ic_x_chan: Option<Vec<(u16, f64)>>,
    pub ic_y_chan: Option<Vec<(u16, f64)>>,
    pub ic_de_chan: Option<Vec<f64>>,
    pub ic_e_chan: Option<Vec<f64>>,
    pub ic_x_detid: Option<u16>,
    pub ic_x_energy: Option<f64>,
    pub ic_y_detid: Option<u16>,
    pub ic_y_energy: Option<f64>,
    pub ic_de_detid: Option<u16>,
    pub ic_de_energy: Option<f64>,
    pub ic_e_detid: Option<u16>,
    pub ic_e_energy: Option<f64>,
    pub ic_tot_energy: Option<f64>,
    pub ic_pos_x: Option<f64>,
    pub ic_pos_y: Option<f64>,
    pub ic_theta: Option<f64>,
    pub ic_phi: Option<f64>,
    pub si_de_detid: Option<u16>,
    pub si_de_energy: Option<f64>,
    pub si_e_detid: Option<u16>,
    pub si_e_energy: Option<f64>,
    pub si_tot_energy: Option<f64>,
    pub si_detno: Option<u16>,
    pub si_theta: Option<f64>,
    pub si_phi: Option<f64>,
    pub q_value: Option<f64>,
    pub theta_cm: Option<f64>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BeamFraction {
    pub s34: f64,
    pub cl34: f64,
    pub ar34: f64,
    pub pileup: f64,
    pub all: f64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Recoils {
    pub data: Vec<Event>,
}
