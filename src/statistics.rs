use rand::Rng;
use rand_distr::{Distribution, Normal};

pub fn randomize<R: Rng>(val: (f64, f64, f64), rng: &mut R) -> f64 {
    if let Ok(norm) = Normal::new(val.0, val.1.abs() + val.2.abs()) {
        norm.sample(rng)
    } else {
        val.0
    }
}

pub fn stats(vals: &[f64]) -> (f64, f64) {
    let mean = vals.iter().sum::<f64>() / (vals.len() as f64);
    let std_dev = (vals.iter().map(|x| (x - mean) * (x - mean)).sum::<f64>()
        / ((vals.len() - 1) as f64))
        .sqrt();
    (mean, std_dev)
}
