use crate::data::{nuc::Nuclide, runs::Energy};
use lazy_static::lazy_static;
use serde::Serialize;
use std::{collections::HashMap, sync::Mutex};

pub const INCLUDE_DATA: bool = false;

lazy_static! {
    pub static ref OUTPUT: Mutex<Output> = Mutex::new(Output::default());
}

#[derive(Debug, Default, Serialize)]
pub struct Output(pub HashMap<Energy, EnergyOutput>);

#[derive(Debug, Serialize)]
pub struct EnergyOutput {
    pub event: Vec<EventOutput>,
    pub cross_section: CrossSectionOutput,
}

#[derive(Debug, Serialize)]
pub struct CrossSectionOutput {
    //pub n_abcx: (f64, f64, f64),
    pub n_x: (f64, f64, f64),
    pub n_x_eff: (f64, f64, f64),
    pub n_a: (f64, f64, f64),
    pub n_b: (f64, f64, f64),
    pub n_c: (f64, f64, f64),
    //pub n_ab_p0: (f64, f64, f64),
    pub int_a: (f64, f64, f64),
    pub int_b: (f64, f64, f64),
    pub int_c: (f64, f64, f64),
    pub f_b: f64,
    pub f_c: f64,
    pub xsec_b: (f64, f64, f64),
    pub xsec_c: (f64, f64, f64),
    pub xsec_x: (f64, f64, f64),
    // this is because serde needs a map key to be stringifiable, but (u32, u32, u32) isn't, so I
    // just decided to use a Vec instead of a HashMap
    pub max_likelihood_data: Vec<(u32, u32, u32, u32)>,
}

#[derive(Debug, Serialize)]
pub struct EventOutput {
    pub run: RunOutput,
    pub si: SiOutput,
    pub ic: IcOutput,
    pub monte_carlo: McOutput,
}

#[derive(Debug, Serialize)]
pub struct RunOutput {
    pub run_name: String,
    pub pressure: f64,
    pub rhoa: f64,
}

#[derive(Debug, Serialize)]
pub struct AngleOutput {
    pub azimuthal: f64,
    pub polar: f64,
}

#[derive(Debug, Serialize)]
pub struct SiEnergyOutput {
    pub de: Option<f64>,
    pub e: Option<f64>,
    pub tot: f64,
}

#[derive(Debug, Serialize)]
pub struct SiOutput {
    pub energy: SiEnergyOutput,
    pub angle: AngleOutput,
}

#[derive(Debug, Serialize)]
pub struct IcChannelOutput {
    pub x: Option<Vec<(u16, f64)>>,
    pub y: Option<Vec<(u16, f64)>>,
    pub de: Option<Vec<f64>>,
    pub e: Option<Vec<f64>>,
}

#[derive(Debug, Serialize)]
pub struct IcEnergyOutput {
    pub x: Option<f64>,
    pub y: Option<f64>,
    pub de: Option<f64>,
    pub e: Option<f64>,
    pub tot: f64,
}

#[derive(Debug, Serialize)]
pub struct IcOutput {
    pub channel: IcChannelOutput,
    pub shifted_channel: IcChannelOutput,
    pub energy: IcEnergyOutput,
    pub angle: Option<AngleOutput>,
}

#[derive(Debug, Serialize)]
pub struct McOutput(pub HashMap<Nuclide, McSpeciesOutput>);

#[derive(Debug, Serialize)]
pub struct McSpeciesOutput {
    pub prob: f64,
    pub sections: HashMap<String, McSectionOutput>,
}

#[derive(Debug, Serialize)]
pub struct McSectionOutput {
    pub data: Option<Vec<f64>>,
    pub gaus_params: (f64, f64),
    pub raw_prob: f64,
}
