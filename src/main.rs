// NOTE: Errors: (value, stat, sys)
//
use anyhow::Result;
use kincal::{Outgoing, ReactionKinematics};
use monte_carlo_ic::{
    data::{
        kinematics::{kin_line, AR34AP, CL34AP, S34AP},
        runs::{
            self as run_data,
            Energy::{self, E1, E2},
            Event, EFF_34CL_34AR,
        },
    },
    filter_events,
    output::{
        AngleOutput, CrossSectionOutput, EnergyOutput, EventOutput, IcChannelOutput,
        IcEnergyOutput, IcOutput, RunOutput, SiEnergyOutput, SiOutput, OUTPUT,
    },
    probs::event_probs,
};
use rand::{thread_rng, Rng};
use std::collections::HashMap;

fn main() -> Result<()> {
    calc_xsec(E1);
    calc_xsec(E2);

    let output = &*OUTPUT.lock().expect("failed to lock OUTPUT for printing");
    println!(
        "{}",
        serde_json::to_string_pretty(output).expect("failed to serialize OUTPUT")
    );

    Ok(())
}

fn calc_xsec(energy: Energy) {
    use monte_carlo_ic::data::kinematics::AR34AP0;

    let recoils = &run_data::RECOILS[&energy];
    let runs = &run_data::RUN_LISTS[&energy];
    let n_x = run_data::NUM_2P[&energy];
    let _xsec_34s = run_data::XSEC_34S[&energy];
    let _p0_line = kin_line(AR34AP0, energy.energy_actual(AR34AP0.0.beam.into()));

    let mut event_output = Vec::new();

    let probs = recoils
        .data
        .iter()
        .filter(|e| filter_events(e))
        // FIXME: Remove this once every run has a pressure
        .filter(|e| {
            run_data::RUN_INFO[e.run_name.as_ref().unwrap()]
                .cap_ic
                .is_some()
        })
        .map(|event| {
            let (probs, mc_output) = event_probs(event, energy);

            let run_output = {
                let run_name = event.run_name.clone().expect("event missing run_name");
                let pressure = run_data::RUN_INFO[&run_name]
                    .cap_ic
                    .expect("RUN_INFO missing cap_ic")
                    .0;
                let rhoa = run_data::RUN_INFO[&run_name].rhoa.0;
                RunOutput {
                    run_name,
                    pressure,
                    rhoa,
                }
            };
            let si_output = {
                let energy = SiEnergyOutput {
                    de: event.si_de_energy,
                    e: event.si_e_energy,
                    tot: event.si_tot_energy.expect("event missing si_tot_energy"),
                };
                let angle = AngleOutput {
                    azimuthal: event.si_theta.expect("event missing si_theta"),
                    polar: event.si_phi.expect("event missing si_phi"),
                };
                SiOutput { energy, angle }
            };
            let ic_output = {
                let channel = IcChannelOutput {
                    x: event.ic_x_chan.clone(),
                    y: event.ic_y_chan.clone(),
                    de: event.ic_de_chan.clone(),
                    e: event.ic_e_chan.clone(),
                };
                //FIXME: use the transformation, instead of copying channel
                let shifted_channel = IcChannelOutput {
                    x: event.ic_x_chan.clone(),
                    y: event.ic_y_chan.clone(),
                    de: event.ic_de_chan.clone(),
                    e: event.ic_e_chan.clone(),
                };
                let energy = IcEnergyOutput {
                    x: event.ic_x_energy,
                    y: event.ic_y_energy,
                    de: event.ic_de_energy,
                    e: event.ic_e_energy,
                    tot: event.ic_tot_energy.expect("event missing ic_tot_energy"),
                };
                let angle = if let (Some(azimuthal), Some(polar)) = (event.ic_theta, event.ic_phi) {
                    Some(AngleOutput { azimuthal, polar })
                } else {
                    None
                };
                IcOutput {
                    channel,
                    shifted_channel,
                    energy,
                    angle,
                }
            };

            event_output.push(EventOutput {
                run: run_output,
                si: si_output,
                ic: ic_output,
                monte_carlo: mc_output,
            });

            probs
        })
        .collect::<Vec<_>>();

    const N_ITER: usize = 1000;
    let mut all_counts = HashMap::new();
    for _ in 0..N_ITER {
        let mut rng = thread_rng();
        let mut counts = HashMap::new();

        for p in &probs {
            let r = rng.gen_range(0.0..=1.0);
            if r < p[&S34AP] {
                *counts.entry(S34AP).or_insert(0) += 1;
            } else if r < p[&S34AP] + p[&CL34AP] {
                *counts.entry(CL34AP).or_insert(0) += 1;
            } else {
                *counts.entry(AR34AP).or_insert(0) += 1;
            }
        }

        *all_counts
            .entry((
                counts.get(&S34AP).copied().unwrap_or(0),
                counts.get(&CL34AP).copied().unwrap_or(0),
                counts.get(&AR34AP).copied().unwrap_or(0),
            ))
            .or_insert(0) += 1;
    }
    let max_counts = all_counts
        .iter()
        .max_by_key(|(_k, v)| *v)
        .expect("failed to get max_counts")
        .0;

    let [int_a, int_b, int_c] = runs
        .iter()
        .map(|x| calc_int(x))
        // sum both elements and square of errors
        .fold(
            [(0.0, 0.0, 0.0), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0)],
            |acc, x| {
                [
                    (
                        (acc[0]).0 + (x[0]).0,
                        f64::sqrt((acc[0]).1 * (acc[0]).1 + (x[0]).1 * (x[0]).1),
                        (acc[0]).2 + (x[0]).2,
                    ),
                    (
                        (acc[1]).0 + (x[1]).0,
                        f64::sqrt((acc[1]).1 * (acc[1]).1 + (x[1]).1 * (x[1]).1),
                        (acc[1]).2 + (x[1]).2,
                    ),
                    (
                        (acc[2]).0 + (x[2]).0,
                        f64::sqrt((acc[2]).1 * (acc[2]).1 + (x[2]).1 * (x[2]).1),
                        (acc[2]).2 + (x[2]).2,
                    ),
                ]
            },
        );

    let n_a = (max_counts.0 as f64, f64::sqrt(max_counts.0 as f64), 0.0);
    let n_b = (max_counts.1 as f64, f64::sqrt(max_counts.1 as f64), 0.0);
    let n_c = (max_counts.2 as f64, f64::sqrt(max_counts.2 as f64), 0.0);

    let n_x_eff = n_x.0 / (EFF_34CL_34AR.0);
    let n_x_eff = (
        n_x_eff,
        n_x_eff.abs()
            * f64::sqrt(
                n_x.1 * n_x.1 / n_x.0 / n_x.0
                    + EFF_34CL_34AR.1 * EFF_34CL_34AR.1 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0,
            ),
        n_x_eff.abs()
            * f64::sqrt(
                n_x.2 * n_x.2 / n_x.0 / n_x.0
                    + EFF_34CL_34AR.2 * EFF_34CL_34AR.2 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0,
            ),
    );

    let xsec_b = n_b.0 / (EFF_34CL_34AR.0 * int_b.0) * 1e27;
    let xsec_b = (
        xsec_b,
        xsec_b.abs()
            * f64::sqrt(
                n_b.1 * n_b.1 / n_b.0 / n_b.0
                    + EFF_34CL_34AR.1 * EFF_34CL_34AR.1 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_b.1 * int_b.1 / int_b.0 / int_b.0,
            ),
        xsec_b.abs()
            * f64::sqrt(
                n_b.2 * n_b.2 / n_b.0 / n_b.0
                    + EFF_34CL_34AR.2 * EFF_34CL_34AR.2 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_b.2 * int_b.2 / int_b.0 / int_b.0,
            ),
    );

    let xsec_c = n_c.0 / (EFF_34CL_34AR.0 * int_c.0) * 1e27;
    let xsec_c = (
        xsec_c,
        xsec_c.abs()
            * f64::sqrt(
                n_c.1 * n_c.1 / n_c.0 / n_c.0
                    + EFF_34CL_34AR.1 * EFF_34CL_34AR.1 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_c.1 * int_c.1 / int_c.0 / int_c.0,
            ),
        xsec_c.abs()
            * f64::sqrt(
                n_c.2 * n_c.2 / n_c.0 / n_c.0
                    + EFF_34CL_34AR.2 * EFF_34CL_34AR.2 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_c.2 * int_c.2 / int_c.0 / int_c.0,
            ),
    );

    let xsec_x = n_x_eff.0 / (EFF_34CL_34AR.0 * int_c.0) * 1e27;
    let xsec_x = (
        xsec_x,
        xsec_x.abs()
            * f64::sqrt(
                n_x_eff.1 * n_x_eff.1 / n_x_eff.0 / n_x_eff.0
                    + EFF_34CL_34AR.1 * EFF_34CL_34AR.1 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_c.1 * int_c.1 / int_c.0 / int_c.0,
            ),
        xsec_x.abs()
            * f64::sqrt(
                n_x_eff.2 * n_x_eff.2 / n_x_eff.0 / n_x_eff.0
                    + EFF_34CL_34AR.2 * EFF_34CL_34AR.2 / EFF_34CL_34AR.0 / EFF_34CL_34AR.0
                    + int_c.2 * int_c.2 / int_c.0 / int_c.0,
            ),
    );

    // this is because serde needs a map key to be stringifiable, but (u32, u32, u32) isn't, so I
    // just decided to use a Vec instead of a HashMap
    let max_likelihood_data: Vec<_> = all_counts
        .into_iter()
        .map(|((a, b, c), x)| (a, b, c, x))
        .collect();
    let cross_section_output = CrossSectionOutput {
        //n_abcx,
        n_x,
        n_x_eff,
        n_a,
        n_b,
        n_c,
        //n_ab_p0,
        int_a,
        int_b,
        int_c,
        f_b: int_b.0 / (int_b.0 + int_c.0),
        f_c: int_c.0 / (int_b.0 + int_c.0),
        xsec_b,
        xsec_c,
        xsec_x,
        max_likelihood_data,
    };

    OUTPUT
        .lock()
        .expect("failed to lock OUTPUT for inserting")
        .0
        .insert(
            energy,
            EnergyOutput {
                event: event_output,
                cross_section: cross_section_output,
            },
        );
}

fn calc_int(run: &str) -> [(f64, f64, f64); 3] {
    let ri = &run_data::RUN_INFO[run];
    let bf = &run_data::BEAM_FRACS[run];
    let scal = &run_data::SCALERS[run];

    // pileup counts twice, but is already counted once in all.
    // (actually, some pile is three, but that's not much)
    let beam_total = scal.1 + bf.pileup;
    let beam_total = (beam_total, f64::sqrt(beam_total), f64::abs(scal.0 - scal.1));

    // a is 34s
    let beam_a = bf.s34 / (bf.s34 + bf.cl34 + bf.ar34) * beam_total.0;
    let beam_a = (
        beam_a,
        beam_a.abs()
            * f64::sqrt(
                1.0 / (bf.s34)
                    + 1.0 / (bf.s34 + bf.cl34 + bf.ar34)
                    + beam_total.1 * beam_total.1 / beam_total.0 / beam_total.0,
            ),
        beam_a.abs() * beam_total.2 / beam_total.0,
    );
    // b is 34cl
    let beam_b = bf.cl34 / (bf.s34 + bf.cl34 + bf.ar34) * beam_total.0;
    let beam_b = (
        beam_b,
        beam_b.abs()
            * f64::sqrt(
                1.0 / (bf.cl34)
                    + 1.0 / (bf.s34 + bf.cl34 + bf.ar34)
                    + beam_total.1 * beam_total.1 / beam_total.0 / beam_total.0,
            ),
        beam_b.abs() * beam_total.2 / beam_total.0,
    );
    // c is 34ar
    let beam_c = bf.ar34 / (bf.s34 + bf.cl34 + bf.ar34) * beam_total.0;
    let beam_c = (
        beam_c,
        beam_c.abs()
            * f64::sqrt(
                1.0 / (bf.ar34)
                    + 1.0 / (bf.s34 + bf.cl34 + bf.ar34)
                    + beam_total.1 * beam_total.1 / beam_total.0 / beam_total.0,
            ),
        beam_c.abs() * beam_total.2 / beam_total.0,
    );

    let int_a = ri.rhoa.0 * beam_a.0;
    let int_a = (
        int_a,
        int_a.abs()
            * f64::sqrt(
                ri.rhoa.1 * ri.rhoa.1 / ri.rhoa.0 / ri.rhoa.0
                    + beam_a.1 * beam_a.1 / beam_a.0 / beam_a.0,
            ),
        int_a.abs()
            * f64::sqrt(
                ri.rhoa.2 * ri.rhoa.2 / ri.rhoa.0 / ri.rhoa.0
                    + beam_a.2 * beam_a.2 / beam_a.0 / beam_a.0,
            ),
    );
    let int_b = ri.rhoa.0 * beam_b.0;
    let int_b = (
        int_b,
        int_b.abs()
            * f64::sqrt(
                ri.rhoa.1 * ri.rhoa.1 / ri.rhoa.0 / ri.rhoa.0
                    + beam_b.1 * beam_b.1 / beam_b.0 / beam_b.0,
            ),
        int_b.abs()
            * f64::sqrt(
                ri.rhoa.2 * ri.rhoa.2 / ri.rhoa.0 / ri.rhoa.0
                    + beam_b.2 * beam_b.2 / beam_b.0 / beam_b.0,
            ),
    );
    let int_c = ri.rhoa.0 * beam_c.0;
    let int_c = (
        int_c,
        int_c.abs()
            * f64::sqrt(
                ri.rhoa.1 * ri.rhoa.1 / ri.rhoa.0 / ri.rhoa.0
                    + beam_c.1 * beam_c.1 / beam_c.0 / beam_c.0,
            ),
        int_c.abs()
            * f64::sqrt(
                ri.rhoa.2 * ri.rhoa.2 / ri.rhoa.0 / ri.rhoa.0
                    + beam_c.2 * beam_c.2 / beam_c.0 / beam_c.0,
            ),
    );

    [int_a, int_b, int_c]
}

fn filter_events_p0(event: &Event, p0_line: &ReactionKinematics) -> bool {
    let predicted_energy = if let kincal::Value::OneVal(e) =
        p0_line.th_to_k(event.si_theta.unwrap(), Outgoing::Ejectile)
    {
        e
    } else {
        panic!("bad kincal value")
    } * 1000.0;
    let measured_energy = event.si_tot_energy.unwrap();
    //println!("{}\t{}", event.si_theta.unwrap(), predicted_energy);
    (measured_energy - predicted_energy).abs() < 500.0
}
