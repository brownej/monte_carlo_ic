pub mod data;
pub mod e_loss;
pub mod output;
pub mod probs;
pub mod statistics;

use data::runs::{Ejectile, Event};

pub fn filter_events(event: &Event) -> bool {
    let ejectile_pass = if let Some(e_type) = event.ejectile_type {
        e_type == Ejectile::Proton
    } else {
        panic!("ejectile type is None");
    };

    // if it is in the beam cone, ignore it. or if it does not have a theta, ignore it
    let recoil_pass = matches!(event.ic_theta, Some(theta) if theta > 0.6);

    // this was already checked when creating the recoils file
    let timing_pass = true;

    ejectile_pass && recoil_pass && timing_pass
}
