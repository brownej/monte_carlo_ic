use crate::{data::nuc::Nuclide, statistics::randomize};
use e15232_srim::{integrate, Target, Thickness};
use rand::{thread_rng, Rng};

pub fn jet(areal_density: (f64, f64, f64)) -> ((Target, Thickness), (Target, Thickness)) {
    let mut rng = thread_rng();

    let depth_tot = 3.0;
    let depth_frac = rng.gen_range(0.0..=1.0);
    let areal_density = randomize(areal_density, &mut rng);

    (
        (
            Target::Helium,
            Thickness::ArealDensity {
                areal_density,
                depth: depth_frac * depth_tot,
            },
        ),
        (
            Target::Helium,
            Thickness::ArealDensity {
                areal_density,
                depth: (1.0 - depth_frac) * depth_tot,
            },
        ),
    )
}

pub fn ic(pressure: (f64, f64, f64)) -> Vec<(Target, Thickness)> {
    let mut rng = thread_rng();

    let pressure = randomize(pressure, &mut rng);
    let temperature = 293.15; //TODO: randomize?

    vec![
        (Target::Mylar, Thickness::Depth { depth: 3e-3 }),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure,
                temperature,
                depth: 20.0,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure,
                temperature,
                depth: 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure,
                temperature,
                depth: 36.6,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure,
                temperature,
                depth: 73.2,
            },
        ),
        (
            Target::Isobutane,
            Thickness::IdealGas {
                pressure,
                temperature,
                depth: 183.0,
            },
        ),
    ]
}

pub fn all_e_losses(
    proj: Nuclide,
    energy: f64,
    targs: &[(Target, Thickness)],
) -> Vec<(Nuclide, Target, Thickness, f64)> {
    let mut de_sum = 0.0;
    let mut losses = Vec::with_capacity(targs.len());

    for targ in targs.iter() {
        let de = integrate(proj.into(), energy - de_sum, targ.0, targ.1);
        de_sum += de.energy_loss;
        losses.push((proj, targ.0, targ.1, de.energy_loss));
    }

    losses
}
