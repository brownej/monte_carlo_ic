pub mod kinematics;
pub mod nuc;
pub mod runs;

pub mod constants {
    /// Boltzmann constant (k_B) \[MeV/K]
    pub const BOLTZMANN: f64 = 8.617333262145e-11;

    /// Atomic Mass Unit (u) \[MeV/c^2]
    pub const ATOMIC_MASS_UNIT: f64 = 931.49410242;
}
