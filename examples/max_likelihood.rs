use monte_carlo_ic::{
    data::runs::{Energy, RECOILS},
    filter_events,
    probs::event_probs,
};
use rand::{thread_rng, Rng};
use std::collections::HashMap;

fn main() {
    let probs = RECOILS[&Energy::E2]
        .data
        .iter()
        .filter(|e| filter_events(e))
        .map(|event| event_probs(event, Energy::E2))
        .collect::<Vec<_>>();

    const N_ITER: usize = 1000;
    let mut all_counts = HashMap::new();
    for _ in 0..N_ITER {
        use monte_carlo_ic::data::kinematics::{AR34AP, CL34AP, S34AP};

        let mut rng = thread_rng();
        let mut counts = HashMap::new();

        for p in &probs {
            let r = rng.gen_range(0.0..=1.0);
            if r < p[&S34AP] {
                *counts.entry(S34AP).or_insert(0) += 1;
            } else if r < p[&S34AP] + p[&CL34AP] {
                *counts.entry(CL34AP).or_insert(0) += 1;
            } else {
                *counts.entry(AR34AP).or_insert(0) += 1;
            }
        }

        *all_counts
            .entry((counts[&S34AP], counts[&CL34AP], counts[&AR34AP]))
            .or_insert(0) += 1;
    }

    for ((a, b, c), count) in all_counts {
        println!("{} {} {} {} {}", a, b, c, a + b + c, count);
    }
}
