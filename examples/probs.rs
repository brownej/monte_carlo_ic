use monte_carlo_ic::{
    data::runs::{Energy, RECOILS},
    filter_events,
    probs::event_probs,
};

fn main() {
    for event in &RECOILS[&Energy::E2].data {
        if filter_events(event) {
            println!("{:#?}", event_probs(event, Energy::E2));
        };
    }
}
