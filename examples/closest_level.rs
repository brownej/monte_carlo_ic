use monte_carlo_ic::{
    data::{
        kinematics::AR34AP,
        nuc::Beam,
        runs::{Energy, Event},
    },
    probs::closest_level_thcm,
};

fn main() {
    println!(
        "{:?}",
        closest_level_thcm(
            &Event {
                si_tot_energy: Some(5.0),
                si_theta: Some(30.0),
                ..Default::default()
            },
            Energy::E2.energy_actual(Beam::Ar34),
            AR34AP,
        )
    );
}
