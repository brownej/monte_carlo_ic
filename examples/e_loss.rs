use e15232_srim::{
    integrate,
    Projectile::Ar34,
    Target::{Aluminum, Helium, Isobutane, Mylar},
    Thickness,
};

fn main() {
    let mut e_curr = 57.0;

    let de = integrate(
        Ar34,
        e_curr,
        Helium,
        Thickness::ArealDensity {
            areal_density: 7.5e18,
            depth: 2.0,
        },
    );
    e_curr -= de.energy_loss;
    println!("He:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(Ar34, e_curr, Aluminum, Thickness::Depth { depth: 40e-6 });
    e_curr -= de.energy_loss;
    println!("Al:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(Ar34, e_curr, Mylar, Thickness::Depth { depth: 3e-3 });
    e_curr -= de.energy_loss;
    println!("Mylar:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(
        Ar34,
        e_curr,
        Isobutane,
        Thickness::IdealGas {
            pressure: 15.0,
            temperature: 300.0,
            depth: 20.0,
        },
    );
    e_curr -= de.energy_loss;
    println!("dead:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(
        Ar34,
        e_curr,
        Isobutane,
        Thickness::IdealGas {
            pressure: 15.0,
            temperature: 300.0,
            depth: 36.6,
        },
    );
    e_curr -= de.energy_loss;
    println!("IC X:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(
        Ar34,
        e_curr,
        Isobutane,
        Thickness::IdealGas {
            pressure: 15.0,
            temperature: 300.0,
            depth: 36.6,
        },
    );
    e_curr -= de.energy_loss;
    println!("IC Y:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(
        Ar34,
        e_curr,
        Isobutane,
        Thickness::IdealGas {
            pressure: 15.0,
            temperature: 300.0,
            depth: 73.2,
        },
    );
    e_curr -= de.energy_loss;
    println!("IC dE:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    let de = integrate(
        Ar34,
        e_curr,
        Isobutane,
        Thickness::IdealGas {
            pressure: 15.0,
            temperature: 300.0,
            depth: 183.0,
        },
    );
    e_curr -= de.energy_loss;
    println!("IC E:\t{:.3}\t{:.3}", de.energy_loss, e_curr);

    println!("End:\t{:.3}", e_curr);
}
